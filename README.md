# Le Registre - A svelte application for WordPress
## About Le Registre
Le Registre (The Register) is a web application based on WordPress developped for the Hotel Pasteur in Rennes.

## Get started

List of WordPress plugin needed :
- *le-registre-wordpress-plugin* (https://gitlab.com/le-registre/le-registre-wordpress-plugin)
- *Advanced Custom Fields* from WPEngine (<= 6.2.9)
- *Advanced Custom Fields PRO* from WPEngine (<= 6.0.3)
- *WPGraphQL* from  WPGraphQL (<= 1.26.0)
- *WPGraphQL for Advanced Custom Fields* from wpgraphql (<= 0.4.1)
- *WPGraphQL Meta Query* from  Jason Bahl (<=0.1.1)

### Clone the repo !
```bash
git clone https://gitlab.com/le-registre/le-registre.git
```

### Install the dependencies…
```bash
cd le-registre
npm install
```

### Prepare the dev environment
Important : you need to have a .env configured for the web app to work.
To do this, you have to duplicate the .env.example and modify the links :
- API_URL to connect the web app to a wordpress instance running *le-registre-wordpress-plugin* 
- THEME_URL linking to the public folder of le-registre repo for static assets (images and icons)

### Run the dev environment
```bash
npm run dev
```

Navigate to [localhost:5000](http://localhost:5000). You should see your app running. Edit a component file in `src`, save it, and reload the page to see your changes.

By default, the server will only respond to requests from localhost. To allow connections from other computers, edit the `sirv` commands in package.json to include the option `--host 0.0.0.0`.

If you're using [Visual Studio Code](https://code.visualstudio.com/) we recommend installing the official extension [Svelte for VS Code](https://marketplace.visualstudio.com/items?itemName=svelte.svelte-vscode). If you are using other editors you may need to install a plugin in order to get syntax highlighting and intellisense.

## Building and running in production mode

To create an optimized version of the app:

```bash
npm run build
```

You can run the newly built app with `npm run start`. This uses [sirv](https://github.com/lukeed/sirv) too.
