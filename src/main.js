import App from "./App.svelte";
const target = document.querySelector("#le-registre");
if (target) {
  const app = new App({
    target: document.querySelector("#le-registre"),
    props: {
      name: "world",
    },
  });
}
export default app;
