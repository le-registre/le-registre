import { writable, derived } from "svelte/store";
import { fr } from "date-fns/locale";
import {
  format,
  startOfMonth,
  endOfMonth,
  addMonths,
  subMonths,
  eachDayOfInterval,
  setDate,
  isSameMonth,
  formatISO,
  getDaysInMonth,
  parseISO,
  isValid,
  startOfToday,
} from "date-fns";

function createTimelineStore() {
  let url = new URL(window.location);
  let date = parseISO(url.searchParams.get("date"));
  let currentDate = startOfToday();
  if (isValid(date)) {
    currentDate = date;
  } else if (
    url.searchParams.get("restore") == "true" &&
    localStorage.getItem("state") != -1
  ) {
    let localStorageDate = localStorage.getItem("date");
    if (localStorageDate) {
      currentDate = parseISO(localStorageDate);
      url.searchParams.set(
        "date",
        format(currentDate, "yyyy-MM-dd", { locale: fr })
      );
      url.searchParams.delete("restore");
      history.pushState({}, "", url);
    }
  }

  const timelineStore = {
    interval: {
      start: startOfMonth(currentDate),
      end: endOfMonth(currentDate),
    },
    date: currentDate,
  };
  const { subscribe, set, update } = writable(timelineStore);
  return {
    subscribe,
    update,
    incrementMonth: () =>
      update((timeline) => {
        timeline.date = addMonths(timeline.date, 1);
        timeline.interval = {
          start: startOfMonth(timeline.date),
          end: endOfMonth(timeline.date),
        };
        return timeline;
      }),
    decrementMonth: () =>
      update((timeline) => {
        timeline.date = subMonths(timeline.date, 1);
        timeline.interval = {
          start: startOfMonth(timeline.date),
          end: endOfMonth(timeline.date),
        };
        return timeline;
      }),
    setDate: (value) =>
      update((timeline) => {
        timeline.date = setDate(timeline.date, value);
        if (!isSameMonth(timeline.date, timeline.interval.start)) {
          timeline.interval = {
            start: startOfMonth(timeline.date),
            end: endOfMonth(timeline.date),
          };
        }
        return timeline;
      }),
    setDateAbs: (datePicked) =>
      update((timeline) => {
        timeline.date = datePicked;
        timeline.interval = {
          start: startOfMonth(timeline.date),
          end: endOfMonth(timeline.date),
        };
        return timeline;
      }),
    reset: () => set(timelineStore),
  };
}
export const timelineStore = createTimelineStore();

timelineStore.subscribe((timeline) => {
  let url = new URL(window.location);
  if (
    !url.searchParams.has("restore") ||
    url.searchParams.get("restore") == "false"
  ) {
    localStorage.setItem(
      "date",
      format(timeline.date, "yyyy-MM-dd", { locale: fr })
    );
    url.searchParams.set(
      "date",
      format(timeline.date, "yyyy-MM-dd", { locale: fr })
    );
    window.history.pushState({}, "", url);
  }
});

export const dayInMonth = derived(timelineStore, ($timelineStore) =>
  format($timelineStore.date, "d", { locale: fr })
);
export const numberOfDay = derived(timelineStore, ($timelineStore) =>
  eachDayOfInterval($timelineStore.interval)
);
export const numberOfDayInMonth = derived(timelineStore, ($timelineStore) =>
  getDaysInMonth($timelineStore.date)
);
export const intervalFormatted = derived(timelineStore, ($timelineStore) => {
  let interval = {
    start: formatISO($timelineStore.interval.start),
    end: formatISO($timelineStore.interval.end),
  };
  return interval;
});

export const currentMonth = derived(timelineStore, ($timelineStore) =>
  format($timelineStore.date, "MMMM", { locale: fr })
);
