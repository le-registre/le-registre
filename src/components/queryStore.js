import {
  isWithinInterval,
  areIntervalsOverlapping,
  isSameDay,
  parse,
  isAfter,
  compareDesc,
  addDays,
} from "date-fns";

import { writable, derived } from "svelte/store";
import { timelineStore } from "./timeline/timelineStore";
function createQueryStore() {
  const queryStore = {
    isFetching: false,
    error: "",
    data: [],
  };
  const { subscribe, set, update } = writable(queryStore);
  return {
    subscribe,
    update,
    updateData: (queryData) =>
      update((query) => {
        query.data = queryData;
        return query;
      }),
    isFetching: (isFetching) =>
      update((query) => {
        query.isFetching = isFetching;
        return query;
      }),
    setError: (error) =>
      update((query) => {
        query.error = error;
        return query;
      }),
    reset: () => set(timelineStore),
  };
}
export const queryStore = createQueryStore();

export const filteredData = derived(
  [queryStore, timelineStore],
  ([$queryStore, $timelineStore]) => {
    let filteredData = [];
    $queryStore.data.forEach((e) => {
      if (
        e.data.multipleDates === null ||
        e.data.isMultDate === null ||
        e.data.isMultDate === false
      ) {
        // If all dates are defined, we check if the date is in visits data interval
        if (
          e.data.isEndDate == "true" &&
          e.data.startDate != undefined &&
          e.data.endDate != undefined
        ) {
          if (
            isWithinInterval($timelineStore.date, {
              start: parse(e.data.startDate, "yyyy-M-d", new Date()),
              end: addDays(parse(e.data.endDate, "yyyy-M-d", new Date()), 0),
            })
          ) {
            filteredData.push(e);
          }
        }
        // If isEndDate is false, we filter only by startDate
        else if (
          e.data.isEndDate == "false" &&
          isAfter(
            $timelineStore.date,
            parse(e.data.startDate, "yyyy-M-d", new Date())
          )
        ) {
          filteredData.push(e);
        }
      } else {
        // If there is multiple dates, we add a new visit for each matched interval
        e.data.multipleDates.forEach((interval, index) => {
          let startDateISO = parse(interval.startDate, "yyyy-M-d", new Date());
          let endDateISO = parse(interval.endDate, "yyyy-M-d", new Date())
          // If the date of multipleDates are reversed, we modify the interval
          if (isAfter(startDateISO, endDateISO)) {
            let oldStartDate = startDateISO;
            endDateISO = startDateISO;
            startDateISO = endDateISO
          }
          if (
            isWithinInterval($timelineStore.date, {
              start: startDateISO,
              end: addDays(endDateISO, 0),
            })
          ) {
            e.data.startDate = interval.startDate;
            e.data.endDate = interval.endDate;
            e.data.visitIndex = index;
            filteredData.push(e);
          }
        });
      }
    });
    return filteredData.sort((a, b) =>
      compareDesc(
        parse(a.data.startDate, "yyyy-M-d", new Date()),
        parse(b.data.startDate, "yyyy-M-d", new Date())
      )
    );
  }
);
export const timelineData = derived(
  [queryStore, timelineStore],
  ([$queryStore, $timelineStore]) => {
    let timelineData = [];
    $queryStore.data.forEach((node) => {
      if (
        node.data.multipleDates === null ||
        node.data.isMultDate === null ||
        node.data.isMultDate === false
      ) {
        timelineData.push({
          title: node.title,
          startDate: node.data.startDate,
          endDate: node.data.endDate,
          isEndDate: node.data.isEndDate,
          color: node.data.color,
          events: node.data.events,
        });
      } else {
        node.data.multipleDates.forEach((interval) => {
          let startDateISO = parse(interval.startDate, "yyyy-M-d", new Date());
          let endDateISO = addDays(
            parse(interval.endDate, "yyyy-M-d", new Date()),
            0
          );
          // If the date of multipleDates are reversed, we modify the interval
          if (isAfter(startDateISO, endDateISO)) {
            let oldStartDate = startDateISO;
            endDateISO = startDateISO;
            startDateISO = endDateISO
          }
          if (
            areIntervalsOverlapping(
              {
                start: startDateISO,
                end: endDateISO,
              },
              $timelineStore.interval
            ) ||
            (isSameDay(startDateISO, endDateISO) &&
              isWithinInterval(startDateISO, $timelineStore.interval))
          ) {
            timelineData.push({
              title: node.title,
              startDate: interval.startDate,
              endDate: interval.endDate,
              isEndDate: "true",
              color: node.data.color,
              events: node.data.events,
            });
          }

        });
      }
    });
    let monthLines = [],
      dayLines = [];
    timelineData.forEach((e) => {
      if (
        (!isWithinInterval(
          parse(e.startDate, "yyyy-M-d", new Date()),
          $timelineStore.interval
        ) &&
          e.isEndDate === "false") ||
        (!isWithinInterval(
          parse(e.startDate, "yyyy-M-d", new Date()),
          $timelineStore.interval
        ) &&
          !isWithinInterval(
            parse(e.endDate, "yyyy-M-d", new Date()),
            $timelineStore.interval
          ))
      ) {
        monthLines.push(e);
      } else {
        dayLines.push(e);
      }
    });
    return {
      dayLines: dayLines,
      monthLines: monthLines,
    };
  }
);
