import { writable } from "svelte/store";
function createmapStore() {
  const floorsNumber = 5;
  const defaultFloor = 1;
  const openedHeight = 400;
  const closedHeight = 300;
  const array = new Array(floorsNumber).fill(0).map((e) => ({
    x: 0,
    y: 0,
    scale: 1,
  }));
  const mapStore = {
    map: {
      isOpen: false,
      isScrolling: false,
      height: closedHeight,
      activeFloor: defaultFloor,
      floorNumber: floorsNumber,
      isNextFloorDisabled: false,
      isPreviousFloorDisabled: false,
      openRatio: 0.4,
      width: 0,
    },
    floors: array
  };
  const { subscribe, set, update } = writable(mapStore);

  return {
    subscribe,
    open: () =>
      update(function (obj) {
        // TODO : change the method using array.prototype.map() instead of for loop
        for (let index = 0; index < obj.floors.length; index++) {
          const element = obj.floors[index];
          element.y =
            (obj.floors.length - 1 - index) * obj.map.width * obj.map.openRatio;
          obj.floors[index] = element;
        }
        obj.map.height = openedHeight;
        obj.map.isOpen = true;
        return obj;
      }),
    updateWidth: (width) =>
      update((obj) => {
        const map = (value, x1, y1, x2, y2) =>
          ((value - x1) * (y2 - x2)) / (y1 - x1) + x2;
        obj.map.width = width;
        if (obj.map.isOpen) {
          let scale = 1;
          let lowBreakPoint = 600,
            highBreakPoint = 1200,
            openRatioMin = 0.25,
            openRatioMax = 0.6,
            scaleMin = 1.2,
            scaleMax = 1.9;
          if (width > lowBreakPoint && width < highBreakPoint) {
            scale = map(
              width,
              lowBreakPoint,
              highBreakPoint,
              scaleMax,
              scaleMin
            );
            obj.map.openRatio = map(
              width,
              lowBreakPoint,
              highBreakPoint,
              openRatioMax,
              openRatioMin
            );
          } else if (width <= lowBreakPoint) {
            obj.map.openRatio = openRatioMax;
            scale = scaleMax;
          } else {
            obj.map.openRatio = openRatioMin;
            scale = scaleMin;
          }

          for (let index = 0; index < obj.floors.length; index++) {
            const element = obj.floors[index];
            element.y =
              (obj.floors.length - 1 - index) *
              obj.map.width *
              obj.map.openRatio;
            element.scale = scale;
            obj.floors[index] = element;
          }
        }
        return obj;
      }),
    close: () =>
      update(function (obj) {
        // TODO : change the method using array.prototype.map() instead of for loop
        for (let index = 0; index < obj.floors.length; index++) {
          const element = obj.floors[index];
          element.y = 0;
          element.scale = 1;
          obj.floors[index] = element;
        }
        obj.map.height = closedHeight;
        obj.map.isOpen = false;
        return obj;
      }),
    moveTo: (index) =>
      update(function (obj) {
        if (obj.map.isOpen && !obj.map.isScrolling) {
          let floor = document.getElementById("floor-" + index);

          obj.map.activeFloor = Number(index);
          floor.scrollIntoView({
            behavior: "smooth",
            block: "center",
            inline: "center",
          });
          obj.map.isNextFloorDisabled = obj.map.activeFloor == floorsNumber - 1;
          obj.map.isPreviousFloorDisabled = obj.map.activeFloor == 0;
        }
        return obj;
      }),
    scrollStart: () =>
      update(function (obj) {
        obj.map.isScrolling == true;
        return obj;
      }),
    scrollEnd: () =>
      update(function (obj) {
        obj.map.isScrolling == false;
        return obj;
      }),
    increment: () =>
      update(function (obj) {
        let floor = document.getElementById(
          "floor-" + (obj.map.activeFloor + 1)
        );
        if (obj.map.activeFloor < obj.floors.length - 1) {
          obj.map.activeFloor += 1;
          floor.scrollIntoView({
            behavior: "smooth",
            block: "center",
            inline: "center",
          });
          // TODO: Optimize this method by parsing selectable floor in mapStore
          obj.map.isNextFloorDisabled = obj.map.activeFloor == floorsNumber - 1;
          obj.map.isPreviousFloorDisabled = obj.map.activeFloor == 0;
        }
        return obj;
      }),
    decrement: () =>
      update(function (obj) {
        let floor = document.getElementById(
          "floor-" + (obj.map.activeFloor - 1)
        );
        if (obj.map.activeFloor > 0) {
          obj.map.activeFloor -= 1;
          floor.scrollIntoView({
            behavior: "smooth",
            block: "center",
            inline: "center",
          });
          obj.map.isNextFloorDisabled = obj.map.activeFloor == floorsNumber - 1;
          obj.map.isPreviousFloorDisabled = obj.map.activeFloor == 0;
        }
        return obj;
      }),
    reset: () => set(mapStore),
  };
}

export const mapStore = createmapStore();
