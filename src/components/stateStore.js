/**
 * This Store handle the state change and store the app's stage both in url params and local storage
 */
import { writable } from "svelte/store";
function createStateStore() {
  let initState = 0;
  // The initialization is in three step, first we see if there is a state in the url param
  let url = new URL(window.location);
  let views = ["home", "map", "agenda"];
  let view = url.searchParams.get("view");
  let urlQueryState = views.findIndex((e) => e == view);
  if (urlQueryState != -1) {
    initState = urlQueryState;
  }
  // If there is a restore param in the url, we search in the local storage 
  else if (
    url.searchParams.get("restore") == "true" &&
    localStorage.getItem("state") != -1
  ) {
    // If there is a state in the local storage, we restore it
    initState = parseInt(localStorage.getItem("state"));
    switch (initState) {
      case 0:
        url.searchParams.set("view", "home");
        break;
      case 1:
        url.searchParams.set("view", "map");
        break;
      case 2:
        url.searchParams.set("view", "agenda");
        break;
      default:
        url.searchParams.set("view", "home");
        break;
    }
    history.pushState({}, "", url);
  }

  // And if there is no state in the local storage, it stay at 0 (home)
  const maxState = 2;
  const initStateStore = {
    state: initState,
    direction: "up",
    prevState: 0
  };
  const { subscribe, set, update } = writable(initStateStore);

  return {
    subscribe,
    increment: () =>
      update((stateStore) => {
        stateStore.prevState = stateStore.state
        if (stateStore.direction == "up") {
          if (stateStore.state < maxState) {
            stateStore.state += 1;
          } else if (stateStore.state >= maxState) {
            stateStore.direction = "down";
            stateStore.state -= 1;
          }
        } else if (stateStore.direction == "down") {
          if (stateStore.state > 1) {
            stateStore.state -= 1;
          } else if (stateStore.state <= 1) {
            stateStore.direction = "up";
            stateStore.state += 1;
          }
        }
        return stateStore;
      }),
    // jumpTo is not currently used
    jumpTo: (stateToJumpTo) =>
      update((stateStore) => {
        if (stateToJumpTo >= 0 && stateToJumpTo <= maxState) {
          stateStore.state = stateToJumpTo;
        }
        return stateStore;
      }),
    reset: () => set(initStateStore),
  };
}

export const stateStore = createStateStore();

/**
 * This derived store change the URL and localStorage of browser accordingly to current's states of the app
 * It's called whenever the Svelte module imports the store and changes its state accordingly.
 */
stateStore.subscribe((stateStore) => {
  let url = new URL(window.location);
  if (
    !url.searchParams.has("restore") ||
    url.searchParams.get("restore") == "false"
  ) {
    localStorage.setItem("state", stateStore.state);
    switch (stateStore.state) {
      case 0:
        url.searchParams.set("view", "home");
        break;
      case 1:
        url.searchParams.set("view", "map");
        break;
      case 2:
        url.searchParams.set("view", "agenda");
        break;
      default:
        url.searchParams.set("view", "home");
        break;
    }
    // updates the URL in the browser history
    history.pushState({}, "", url);
  }
});
